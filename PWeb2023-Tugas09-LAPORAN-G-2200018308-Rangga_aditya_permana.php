<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>PHP</title>
</head>
<body>
<?php
$gaji = 1000000;
$pajak = 0.1;
$thp = $gaji - ($gaji * $pajak);
echo "Gaji sebelum pajak = Rp. " . number_format($gaji) . "<br>";
echo "Gaji yang dibawa pulang = Rp. " . number_format($thp) . "<br>";
// Pernyataan kondisional
$a = 5;
$b = 4;
echo "<br>Pernyataan Kondisional:<br>";
echo "$a == $b : " . ($a == $b) . "<br>";
echo "$a != $b : " . ($a != $b) . "<br>";
echo "$a > $b : " . ($a > $b) . "<br>";
echo "$a < $b : " . ($a < $b) . "<br>";
echo "($a == $b) && ($a > $b) : " . (($a != $b) && ($a > $b)) . "<br>";
echo "($a == $b) || ($a > $b) : " . (($a != $b) || ($a > $b)) . "<br>";
// Perulangan
echo "<br>Contoh Perulangan:<br>";
echo "Angka dari 1 hingga 5:<br>";
for ($i = 1; $i <= 5; $i++) {
 echo $i . "<br>";
}
// Array
echo "<br>Contoh Array:<br>";
$buah = array("Apel", "Pisang", "Jeruk");
echo "Buah-buahan: " . implode(", ", $buah) . "<br>";
// Fungsi
echo "<br>Contoh Fungsi:<br>";
function hitungKuadrat($angka) {
 return $angka * $angka;
}
$angka = 6;
$kuadrat = hitungKuadrat($angka);
echo "Kuadrat dari $angka adalah: $kuadrat<br>";
?>
</body>
</html>
